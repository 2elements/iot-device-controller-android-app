package com.example.deepak.iot;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.orm.SugarApp;

import java.net.CookieHandler;
import java.net.CookieManager;

public class Config extends SugarApp {

    private static Config instance;
    private static RequestQueue volleyRequestQueue;
    private static String clientId;
    private static String ApiURL;
    private static String MY_PREFS = "IOT_MY_PREFS";

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        volleyRequestQueue = Volley.newRequestQueue(Config.getInstance());
        clientId = "77004ea213d5fc71acf74a8c9c6795fb";
        ApiURL = "http://139.59.49.43:3000";

        CookieManager cookiemanager = new java.net.CookieManager();
        CookieHandler.setDefault(cookiemanager);
    }

    public static Config getInstance() {
        return instance;
    }

    public static RequestQueue getVolleyRequestQueue() {
        return volleyRequestQueue;
    }

    public static String getClientId() {
        return clientId;
    }

    public static String getApiURL() {
        return ApiURL;
    }

    public static String getMyPrefs() {
        return MY_PREFS;
    }
}
