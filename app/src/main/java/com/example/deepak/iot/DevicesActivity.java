package com.example.deepak.iot;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.deepak.iot.adapters.DeviceAdapter;
import com.example.deepak.iot.models.Device;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class DevicesActivity extends AppCompatActivity {

    private JSONArray devicesArray;
    private ArrayAdapter mListAdapter;
    private String locationId;
    private IntentIntegrator qrScan;
    private AlertDialog.Builder builder;
    private EditText deviceNameInput;
    private EditText deviceNodeIdInput;
    private EditText deviceIdInput;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);

        Intent intent = getIntent();
        String locationString = intent.getStringExtra("LOCATION_DATA");
        JSONObject locationObject;
        try {
            locationObject = new JSONObject(locationString);
            locationId = locationObject.getString("_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        getDevices();
        addNewDevices();
    }

    /**
     * Function to create device list view from device array.
     */
    void createDevicesList() {
        ListView listview = (ListView) findViewById(R.id.devices_list);

        ArrayList<Device> devicesArrayList = new ArrayList<Device>();

        for(int i = 0; i < devicesArray.length(); i++)
        {
            try {
                JSONObject objects = devicesArray.getJSONObject(i);
                String name = objects.getString("name");
                String nodeId = objects.getString("nodeId");
                String deviceId = objects.getString("deviceId");
                String state = "0";

                Device d = new Device(name, nodeId, deviceId, state);
                devicesArrayList.add(d);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Iterate through the elements of the array i.
            //Get thier value.
            //Get the value for the first element and the value for the last element.
        }
        //devicesArrayList.add(new Device("Device 1", "13854806", "1", "0"));
        //devicesArrayList.add(new Device("Device 2", "13854806", "2", "0"));
        //devicesArrayList.add(new Device("Device 3", "13854806", "3", "0"));
        DeviceAdapter deviceAdapter = new DeviceAdapter(this, devicesArrayList);

        //ArrayList<Device> newDevice = Device.fromJson(devicesArray);
        //deviceAdapter.addAll(devicesArrayList);

        listview.setAdapter(deviceAdapter);
    }

    /**
     * Function to fetch all devices of a location from server
     */
    private void getDevices() {
        String url = Config.getApiURL() + "/users/devices";
        String uri = Uri.parse(url)
                .buildUpon()
                .build().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.i("[#]RESPONSE: devices:", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String responseCode = jsonObject.getString("code");

                            if (responseCode.equals("1")) {
                                devicesArray = jsonObject.getJSONArray("data");
                                createDevicesList();
                            } else {
                                Snackbar.make(findViewById(android.R.id.content), jsonObject.getString("message"), Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Snackbar.make(findViewById(android.R.id.content), "Please try again later!", Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("[#]ERROR RESPONSE", String.valueOf(error));
                Snackbar.make(findViewById(android.R.id.content), "Please check your internet connection.", Snackbar.LENGTH_LONG)
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("locationId", locationId);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        Config.getVolleyRequestQueue().add(stringRequest);
    }

    /**
     * Function to create dialog for adding a new device
     */
    void addNewDevices() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add_devices);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                initializeBuilder();
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    /**
     * Function to initialize builder elements
     */
    private void initializeBuilder() {
        //Builder and builder inputs initialized
        builder = new AlertDialog.Builder(DevicesActivity.this);
        builder.setTitle("Add New Device");

        Context context = builder.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View builderView = inflater.inflate(R.layout.add_device_form, null, false);

        deviceNameInput = (EditText) builderView.findViewById(R.id.device_name);
        deviceNodeIdInput = (EditText) builderView.findViewById(R.id.device_node_id);
        deviceIdInput = (EditText) builderView.findViewById(R.id.device_id);

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String deviceName = deviceNameInput.getText().toString();
                String deviceNodeId = deviceNodeIdInput.getText().toString();
                String deviceId = deviceIdInput.getText().toString();

                saveDevice(deviceName, deviceNodeId, deviceId);
                getDevices();
                mListAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setView(builderView);

        qrScan = new IntentIntegrator(this);

        Button scanQrCodeBtn = (Button) builderView.findViewById(R.id.btn_scan_qr_code);
        scanQrCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrScan.initiateScan();
            }
        });
    }

    /**
     * Function to post new device detail to the server
     *
     * @param name     Name of new device
     * @param nodeId   Node id of new device
     * @param deviceId Device if of new device
     */
    private void saveDevice(final String name, final String nodeId, final String deviceId) {

        String url = Config.getApiURL() + "/users/devices";
        String uri = Uri.parse(url)
                .buildUpon()
                .build().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.i("[#]RESPONSE", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String responseCode = jsonObject.getString("code");

                            if (responseCode.equals("1")) {
                                JSONObject data = jsonObject.getJSONObject("data");
                            } else {
                                Snackbar.make(findViewById(android.R.id.content), jsonObject.getString("message"), Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Snackbar.make(findViewById(android.R.id.content), "Please try again later!", Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("[#]ERROR RESPONSE", String.valueOf(error));
                Snackbar.make(findViewById(android.R.id.content), "Please check your internet connection.", Snackbar.LENGTH_LONG)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("nodeId", nodeId);
                params.put("deviceId", deviceId);
                params.put("locationId", locationId);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        Config.getVolleyRequestQueue().add(stringRequest);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                String scanData = result.getContents();
                StringTokenizer tokens = new StringTokenizer(scanData, ",");
                deviceNodeIdInput.setText(tokens.nextToken());
                deviceIdInput.setText(tokens.nextToken());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
