package com.example.deepak.iot.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deepak on 30/07/18.
 */

public class Device {

    public String name;
    public String nodeId;
    public String deviceId;
    public String state;

    public Device(JSONObject object) {
        try {
            this.name = object.getString("name");
            //this.state = object.getBoolean("state");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Device(String name, String nodeId, String deviceId, String state) {
        this.name = name;
        this.state = state;
        this.nodeId = nodeId;
        this.deviceId = deviceId;
    }

    public static ArrayList<Device> fromJson(JSONArray jsonObjects) {
        ArrayList<Device> devices = new ArrayList<Device>();
        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                devices.add(new Device(jsonObjects.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return devices;
    }
}
