package com.example.deepak.iot;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText unameEditText;
    private EditText passEditText;
    private Button signInBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        unameEditText = (EditText) findViewById(R.id.user_email);
        passEditText = (EditText) findViewById(R.id.user_pwd);
        signInBtn = (Button) findViewById(R.id.sign_in_btn);

        GradientDrawable gd = new GradientDrawable();
        gd.setColor(Color.WHITE);
        gd.setCornerRadius(10);
        signInBtn.setBackground(gd);

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uname = unameEditText.getText().toString();
                String pass = passEditText.getText().toString();

                if (uname.equals("")) {
                    unameEditText.setError("Please enter a username or email");
                    return;
                }

                if (pass.equals("")) {
                    passEditText.setError("Please enter the password");
                    return;
                }

                login(uname, pass);
            }
        });

        TextView goToRegister = (TextView) findViewById(R.id.go_to_register);
        goToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }



    private void login(final String uname, final String pass) {

        String url = Config.getApiURL() + "/users";
        String uri = Uri.parse(url)
                .buildUpon()
                .build().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.i("[#]RESPONSE", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String responseCode = jsonObject.getString("code");

                            if(responseCode.equals("1")) {
                                JSONObject data = jsonObject.getJSONObject("data");
                                String userName = data.getString("username");
                                String password = data.getString("password");

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Snackbar.make(findViewById(android.R.id.content), jsonObject.getString("message"), Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Snackbar.make(findViewById(android.R.id.content), "Please try again later!", Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("[#]ERROR RESPONSE", String.valueOf(error));
                Snackbar.make(findViewById(android.R.id.content), "Please check your internet connection.", Snackbar.LENGTH_LONG)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", uname);
                params.put("password", pass);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        Config.getVolleyRequestQueue().add(stringRequest);

    }


}
