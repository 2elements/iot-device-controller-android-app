package com.example.deepak.iot.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.deepak.iot.Config;
import com.example.deepak.iot.R;
import com.example.deepak.iot.models.Device;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by deepak on 30/07/18.
 */

public class DeviceAdapter extends ArrayAdapter<Device> {

    public DeviceAdapter(Context context, ArrayList<Device> devices) {
        super(context, 0, devices);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Device device = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.devices_listview, parent, false);
        }
        // Lookup view for data population
        TextView deviceName = (TextView) convertView.findViewById(R.id.device_display_name);
        Switch deviceState = (Switch) convertView.findViewById(R.id.device_state);
        // Populate the data into the template view using the data object
        deviceName.setText(device.name);
        deviceState.setChecked(device.state.equals("1"));
        // Return the completed view to render on screen

        final View finalConvertView = convertView;
        deviceState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.i(device.name, String.valueOf(b));
                switchDevice(device, b, finalConvertView);
            }
        });
        return convertView;
    }


    private void switchDevice(final Device device, final boolean state, final View view) {

        String url = Config.getApiURL() + "/node/state";
        String uri = Uri.parse(url)
                .buildUpon()
                .build().toString();

        Log.i("Modifying NID", device.nodeId);
        Log.i("Modifying DID", device.deviceId);
        Log.i("Modifying STATE", String.valueOf(state));

        StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.i("[#]RESPONSE", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String responseCode = jsonObject.getString("code");

                            if (responseCode.equals("1")) {
                                Snackbar.make(view.getRootView().findViewById(android.R.id.content), "State Changed!", Snackbar.LENGTH_LONG)
                                        .show();
                            } else {
                                Snackbar.make(view.getRootView().findViewById(android.R.id.content), jsonObject.getString("message"), Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Snackbar.make(view.getRootView().findViewById(android.R.id.content), "Please try again later!", Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("[#]ERROR RESPONSE", String.valueOf(error));
                Snackbar.make(view.getRootView().findViewById(android.R.id.content), "Please check your internet connection.", Snackbar.LENGTH_LONG)
                        .show();
            }
        }) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nodeId", device.nodeId);
                params.put("deviceId", device.deviceId);
                params.put("state", state ? "1" : "0");
                return params;
            }
        };
        // Add the request to the RequestQueue.
        Config.getVolleyRequestQueue().add(stringRequest);

    }


}